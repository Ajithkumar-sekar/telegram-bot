
PROJECT DETAILS
===============

[click here](https://www.youtube.com/watch?v=zTJ1JtZPuAA) for project demonstration

SCREEN SHOT
-----------
![SCREENSHOT](http://s33.postimg.org/596mj45dr/tele_bot_img.png)

###Inspired by
* [Hackers script](https://github.com/NARKOZ/hacker-scripts)
+ [Quora] (https://www.quora.com/What-are-the-best-Python-scripts-youve-ever-written)


###APIs used
1. [Telegram Bot API](https://core.telegram.org/bots/api)
2. [Google calendar API](https://developers.google.com/google-apps/calendar/)
3. [Google youtube Data API](https://developers.google.com/youtube/v3/)
4. [Dictionary API](http://developer.wordnik.com/docs.html#!/word)

###Features to be implemented further
* Natural language processing with python
